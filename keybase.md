### Keybase proof

I hereby claim:

  * I am francisrstokes on github.
  * I am francisstokes (https://keybase.io/francisstokes) on keybase.
  * I have a public key ASDAQWguSwFKdWeKeEvEeTyzezi0na5Pmhdva4wN2Cwj4wo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0120c041682e4b014a75678a784bc4793cb37b38b49dae4f9a176f6b8c0dd82c23e30a",
      "host": "keybase.io",
      "kid": "0120c041682e4b014a75678a784bc4793cb37b38b49dae4f9a176f6b8c0dd82c23e30a",
      "uid": "8b6adb28871a39591ca88a3ecd8d1a19",
      "username": "francisstokes"
    },
    "merkle_root": {
      "ctime": 1576833392,
      "hash": "79807f654b3d2b5b2a5866756b2458cb3b2160f66e0a8765ed35a27842463df09d379f3063af3b60c50f8c95f28f6841d3df126a32eacbce232f013a8de9ed45",
      "hash_meta": "bb6360860ac16deb7948825d4f63750ed45bf2f08bf3ec20f89bee7b2db735b7",
      "seqno": 13850849
    },
    "service": {
      "entropy": "4TKVHZ47tbkTFSoDGhrnpBNh",
      "name": "github",
      "username": "francisrstokes"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.1.1"
  },
  "ctime": 1576833400,
  "expire_in": 504576000,
  "prev": "219fab1d59aae3b26ff366e2e7820d524e56218b19359b463caeac856bc9ed08",
  "seqno": 4,
  "tag": "signature"
}
```

with the key [ASDAQWguSwFKdWeKeEvEeTyzezi0na5Pmhdva4wN2Cwj4wo](https://keybase.io/francisstokes), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgwEFoLksBSnVninhLxHk8s3s4tJ2uT5oXb2uMDdgsI+MKp3BheWxvYWTESpcCBMQgIZ+rHVmq47Jv82bi54INUk5WIYsZNZtGPK6shWvJ7QjEIGP4QySxPOvC4PLtiPFn0S64S51ukiLw79PW6kX8Dk8WAgHCo3NpZ8RA48ci/13KBdngpli1P9M7aZBEsy/GY4fCix6H++BrbqBrt/hOKWZzFISNS16moWFAU7z8/DriVr8yKLJ8I1x2AahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIOznhom8o4be97lg4/INDIccG/cIGnS5qEeNdQLJ262So3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/francisstokes

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id francisstokes
```